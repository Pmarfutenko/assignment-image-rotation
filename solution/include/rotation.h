//
// Created by Petr Marfutenko on 05.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_ROTATION_H

#include "img.h"

struct image rotate( struct image const source );

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_ROTATION_H
