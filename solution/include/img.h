//
// Created by Petr Marfutenko on 05.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_IMG_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_IMG_H
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum open_files_status{
    OPEN_FILE_OK,
    OPEN_FILE_ERROR,
};

enum close_files_status{
    CLOSE_FILE_OK,
    CLOSE_FILE_ERROR,
};

void img_clean_up(struct image* init_img, struct image* rot_img);

enum open_files_status file_open(FILE** file, const char* path, const char* user_rights);
enum close_files_status close_files_to_rotate(FILE** in);
#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_IMG_H
