//
// Created by Petr Marfutenko on 05.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_HEADER_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_HEADER_H

#include "img.h"
#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_WRONG_SIZE_OF_HEADER,
    READ_INVALID_NUMBERS_OF_PLANES,
    READ_POINTER_IS_NULL
    /* коды других ошибок  */
};


enum read_status from_bmp( FILE* in, struct image* img );


/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};


enum write_status to_bmp( FILE* out, struct image const* img );

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_HEADER_H
