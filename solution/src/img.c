//
// Created by Petr Marfutenko on 05.01.2022.
//
#include "img.h"
#include <stdio.h>
#include <stdlib.h>

struct image;

void img_clean_up(struct image* init_img, struct image* rot_img){
    free(init_img->data);
    free(rot_img->data);
}

enum open_files_status file_open(FILE** file, const char* path, const char* user_rights) {
    *file = fopen(path, user_rights);
    if(!(*file)){
        return OPEN_FILE_ERROR;
    }
    return OPEN_FILE_OK;
}


enum close_files_status close_files_to_rotate(FILE** in){
    if(!fclose(*in)) return CLOSE_FILE_ERROR;
    return CLOSE_FILE_OK;
}
