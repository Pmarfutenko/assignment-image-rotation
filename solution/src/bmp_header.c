//
// Created by Petr Marfutenko on 05.01.2022.
//

#include "bmp_header.h"
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)

static const uint16_t BMP_FILE_SIGNATURE = 0x4d42;
static const uint32_t HEADER_INFO_SIZE = 40;
static const uint16_t BITS_PER_PIXEL = 24;

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t bmp_bite_padding(const struct image *img){
    return (img -> height) * (img->width % 4);
}

static struct bmp_header create_header(const struct image* img) {
    return (struct bmp_header) {
            .bfType = BMP_FILE_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * img->width * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_INFO_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = 0,
            .biSizeImage = bmp_bite_padding(img),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
}

enum read_status from_bmp( FILE* in, struct image* img ){

    if(in == NULL) return READ_POINTER_IS_NULL;
    struct bmp_header bmp_header = {0};
    fread(&bmp_header, 1, sizeof(struct bmp_header), in);

    //Проверяем заголовок
    if(bmp_header.bfType != 0x4D42) return READ_INVALID_SIGNATURE;
    if(bmp_header.bOffBits != sizeof(struct bmp_header)) return READ_WRONG_SIZE_OF_HEADER;
    if(bmp_header.biSize != 40) return READ_INVALID_HEADER;
    if(bmp_header.biPlanes != 1) return READ_INVALID_NUMBERS_OF_PLANES;
    if(bmp_header.biBitCount != 24) return READ_INVALID_BITS;

    //Задаем параметры для image
    img->width = bmp_header.biWidth;
    img->height = bmp_header.biHeight;
    img->data = (struct pixel*) malloc(img->width * img->height * sizeof(struct pixel));


    for (size_t i = 0; i < img->height; i++) {
        fread(&(img->data[i*img->width]), sizeof(struct pixel), img->width, in);
        fseek(in, (uint8_t)(img->width  % 4), SEEK_CUR);
    }

    return READ_OK;
}



enum write_status to_bmp( FILE* out, struct image const* img ){

    if(!out){
        return WRITE_ERROR;
    }

    struct bmp_header header = create_header(img);

    fwrite(&header, sizeof(struct bmp_header), 1, out);

    const uint32_t waste = 0;
    const uint8_t padding = (img -> width) % 4;
    for (size_t i = 0; i < img -> height; i++)
        if (!fwrite(&(img -> data[i * (img -> width)]), sizeof(struct pixel),img -> width, out) ||
            !fwrite(&waste, 1, padding, out))
            return WRITE_ERROR;

    return WRITE_OK;
}
