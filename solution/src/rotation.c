//
// Created by Petr Marfutenko on 05.01.2022.
//

#include "img.h"
#include "rotation.h"
#include <stdlib.h>
struct image rotate( struct image const source ) {

    struct image img = {
            .width = source.height,
            .height = source.width,
    };

    img.data = malloc(img.width * img.height * (sizeof(struct pixel)));

    //код для переворота честно найден на просторе Всемирной Сети Интернет
    for (size_t height = 0; height < source.height; height++) {
        for (size_t width = 0; width < source.width; width++) {
            img.data[(img.width - height - 1) + width * img.width] = source.data[height * (source.width) + width];
        }
    }
    return img;
}
