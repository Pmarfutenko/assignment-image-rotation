
#include "bmp_header.h"
#include "../../tester/include/io.h"
#include "img.h"
#include "rotation.h"
#include <stdio.h>
 //Я ваш заголовочный файл для вывода ошибок повзаимствую

//Подумал, что чтобы выводить ошибки по-человечески через ваш заголовочный файл надо давать ему то, что он хочет
const char* status_to_string(enum read_status v){
    switch(v)
    {
        case READ_INVALID_SIGNATURE:
            return "READ_INVALID_SIGNATURE";
        case READ_INVALID_BITS:
            return "READ_INVALID_BITS";
        case READ_INVALID_HEADER:
            return "READ_INVALID_HEADER";
        case READ_WRONG_SIZE_OF_HEADER:
            return "READ_WRONG_SIZE_OF_HEADER";
        case READ_INVALID_NUMBERS_OF_PLANES:
            return "READ_INVALID_NUMBERS_OF_PLANES";
        case READ_POINTER_IS_NULL:
            return "READ_POINTER_IS_NULL";
        case READ_OK: //на всякий случай
            break;
        default: return "Error, idk";

    }
    return "READ_OK";
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3){             //взял из кода проверки, вроде логично
        err("Wrong args\n");
        return 1;
    }

    FILE *input;
    FILE *output;
    enum open_files_status openStatus = file_open(&input, argv[1], "rb");
    if (openStatus == OPEN_FILE_ERROR) {
        fprintf(stderr, "Ошибка открытия файла ввода");
        return 1;
    }

    enum open_files_status openStatusOutput = file_open(&output, argv[2], "wb");
    if (openStatusOutput == OPEN_FILE_ERROR) {
        fprintf(stderr, "Ошибка открытия файла вывода");
        return 1;
    }


    //Эти проверки тоже из кода тестера, по сути провекра открылся ли файл,
    //если файл не отрылся то сработает проверка на NULL в указателе
    if(!input){
        err("Input pointer is null\n");
        return 1;
    }

    if(!output){
        err("Output pointer is null\n");
        return 1;
    }

    struct image initial_img = {0};

    //Тут если возвращается не OK=0, то любое значение будет TRUE, то есть сработает обработчик
    enum read_status read_status = from_bmp(input, &initial_img);
    if(read_status){
        err(status_to_string(read_status));
    }

    struct image rotated_img = rotate(initial_img);

    //Аналогично с проверкой from_bmp

    if(to_bmp(output, &rotated_img)){
        err("WRITE_ERROR\n");
    }

    //Почистим остатки
    close_files_to_rotate(&input);
    close_files_to_rotate(&output);
    img_clean_up(&initial_img, &rotated_img);

    return 0;
}



